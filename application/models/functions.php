<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends CI_Model {

	public function call($param)
	{
		//extract data from the post
		extract($_POST);
		$fields_string = '';
		
		//set POST variables
		$url = 'http://192.241.204.161/api/';
		$fields = array
		(
			'param'=>urlencode($param)
		);
		
		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&amp;'; }
		rtrim($fields_string,'&amp;');
		
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		
		//execute post
		$result = curl_exec($ch);
		$status = curl_getinfo($ch);
		
		//close connection
		curl_close($ch);
		
		return $result;
	}
	
}