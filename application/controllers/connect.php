<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Connect extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Verify Login
		if (!$this->session->userdata('corona_login'))
		{
			//Session Data
			$this->session->set_userdata('corona_login', false);
			$this->session->set_userdata('corona_admin', array());
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('connect/login_view');
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Redirect Dashboard
			redirect(base_url().'dashboard');
		}
	}
	
	public function doLogin()
	{
		//Recibimos las Variables del Formulario
		$username = (isset($_POST['inputUsername'])) ? (string)trim($_POST['inputUsername']) : '';
		$password = (isset($_POST['inputPassword'])) ? (string)trim($_POST['inputPassword']) : '';
		
		//Verificamos que haya Datos
		if ((string)trim($username) != '' && (string)trim($password) != '')
		{
			//Generamos la Llamada
			$array = array(
				'msg' => 'doLogin',
				'fields' => array(
					'username' => (string)trim($username),
					'password' => (string)trim($password)
				),
				'app' => 'backend',
				'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
			);
			$json_array = json_encode($array);
			
			//Respuesta de la Llamada
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
			
			//Verificamos la Llamada
			if ((int)$response_row['status'] == 1)
			{
				//Leemos la Respuesta
				$data = $response_row['data'];
				
				//Generamos el Arreglo de Session
				$session = array(
					'corona_login' => TRUE,
					'corona_admin' => $data
				);
			
				//Asignamos el Arreglo a la Session de la App
				$this->session->set_userdata($session);
				
				//Revisamos si hay una URL para redirigir
				if ((string)trim($this->session->userdata('redirect')) != '')
				{
					//Redirigimos 
					redirect((string)trim($this->session->userdata('redirect')));
				}
				else
				{
					//Redirigimos al Login
					redirect(base_url().'dashboard');
				}
				
				//Vaciamos el Redirect
				$this->session->set_userdata('redirect', '');
			}
			else
			{
				//Leemos el Error de Login
				$this->session->set_userdata('error_login', $response_row['msg']);
				
				//Regresamos al Home
				redirect(base_url());
			}
		}
		else
		{
			//Leemos el Error de Login
			$this->session->set_userdata('error_login', 'Debes escribir tu usuario del sistema y contraseña.');
				
			//Regresamos al Home
			redirect(base_url());
		}
	}
	
	public function forgot()
	{
		//Verify Login
		if (!$this->session->userdata('corona_login'))
		{
			//Session Data
			$this->session->set_userdata('corona_login', false);
			$this->session->set_userdata('corona_admin', array());
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('connect/forgot_view');
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Redirect Dashboard
			redirect(base_url().'dashboard');
		}
	}
	
	public function resetPassword()
	{
		//Recibimos las Variables del Formulario
		$email = (isset($_POST['inputEmail'])) ? (string)trim($_POST['inputEmail']) : '';
		
		//Verificamos que haya Datos
		if ((string)trim($email) != '')
		{
			//Generamos la Llamada
			$array = array(
				'msg' => 'resetPassword',
				'fields' => array(
					'email' => (string)trim($email)
				),
				'app' => 'backend',
				'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
			);
			$json_array = json_encode($array);
			
			//Respuesta de la Llamada
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
			
			//Verificamos la Llamada
			if ((int)$response_row['status'] == 1)
			{
				//Leemos la Respuesta
				$data = $response_row['data'];
				
				//Leemos el Success de Forgot
				$this->session->set_userdata('forgot_success', 'Se ha enviado tu nueva contraseña a tu correo electrónico.');
				
				//Regresamos al Forgot
				redirect(base_url().'connect/forgot');
			}
			else
			{
				//Leemos el Error de Forgot
				$this->session->set_userdata('forgot_error', $response_row['msg']);
				
				//Regresamos al Forgot
				redirect(base_url().'connect/forgot');
			}
		}
		else
		{
			//Leemos el Error de Forgot
			$this->session->set_userdata('forgot_error', 'Debes escribir tu correo electrónico.');
				
			//Regresamos al Forgot
			redirect(base_url().'connect/forgot');
		}
	}
	
	public function logout()
	{
		//Destruimos la Session
		$this->session->sess_destroy();
		
		//Redirigimos al Login
		redirect(base_url());
	}
}