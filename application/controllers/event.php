<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Redirect Dashboard
		redirect(base_url().'dashboard');
	}
	
	public function details()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos Code
			$code = $this->uri->segment(3,0);
			
			//Revisamos 
			if ($code != 0)
			{
				//Generamos la Llamada
				$array = array(
					'msg' => 'getDays',
					'fields' => array(
						'code' => $code
					),
					'app' => 'backend',
					'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
				);
				$json_array = json_encode($array);
				
				//Respuesta de la Llamada
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
				
				//Data
				if ($response_row['status'] == 1)
				{
					$data['days'] = $response_row['data'];
				}
				else
				{
					$data['days'] = array();
				}
				$data['code'] = $code;
				
				//Load Views
				$this->load->view('includes/header');
				$this->load->view('event/detail_view', $data);
				$this->load->view('includes/javascript');
				$this->load->view('includes/footer');
			}
			else
			{
				//Leemos Current URL
				$this->session->set_userdata('redirect', current_url());
				
				//Redirect Dashboard
				redirect(base_url());
			}
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function lineup()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos Code
			$event = $this->uri->segment(3,0);
			$day = $this->uri->segment(3,0);
			
			//Revisamos 
			if ($code != 0 && $day != 0)
			{
				//Generamos la Llamada
				$array = array(
					'msg' => 'getLineup',
					'fields' => array(
						'event' => $code,
						'day' => $day
					),
					'app' => 'backend',
					'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
				);
				$json_array = json_encode($array);
				
				//Respuesta de la Llamada
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
				
				//Data
				if ($response_row['status'] == 1)
				{
					$data['days'] = $response_row['data'];
				}
				else
				{
					$data['days'] = array();
				}
				$data['code'] = $code;
				
				//Load Views
				$this->load->view('includes/header');
				$this->load->view('event/detail_view', $data);
				$this->load->view('includes/javascript');
				$this->load->view('includes/footer');
			}
			else
			{
				//Leemos Current URL
				$this->session->set_userdata('redirect', current_url());
				
				//Redirect Dashboard
				redirect(base_url());
			}
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function add()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('event/new_view');
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
}