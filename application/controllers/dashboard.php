<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('dashboard/index_view');
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function addImage()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos el Valor
			$idphoto = (isset($_POST['idphoto'])) ? (int)$_POST['idphoto'] : 0;
			
			//Verificamos
			if ($idphoto != 0)
			{
				//Generamos la Llamada
				$array = array(
					'msg' => 'addMedia',
					'fields' => array(
						'idphoto' => $idphoto
					),
					'app' => 'backend',
					'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
				);
				$json_array = json_encode($array);
				
				//Respuesta de la Llamada
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
				
				//Data
				if ($response_row['status'] == 1)
				{
					//Regresamos Error
					echo 'success';
				}
				else
				{
					//Regresamos Error
					echo $response_row['msg'];
				}
			}
			else
			{
				//Regresamos Error
				echo 'error';
			}
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function deleteImage()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos el Valor
			$idphoto = (isset($_POST['idphoto'])) ? (int)$_POST['idphoto'] : 0;
			
			//Verificamos
			if ($idphoto != 0)
			{
				//Generamos la Llamada
				$array = array(
					'msg' => 'deleteMedia',
					'fields' => array(
						'idphoto' => $idphoto
					),
					'app' => 'backend',
					'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
				);
				$json_array = json_encode($array);
				
				//Respuesta de la Llamada
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
				
				//Data
				if ($response_row['status'] == 1)
				{
					//Regresamos Error
					echo 'success';
				}
				else
				{
					//Regresamos Error
					echo $response_row['msg'];
				}
			}
			else
			{
				//Regresamos Error
				echo 'error';
			}
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function images()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos la Variable
			$offset = $this->uri->segment(3,0);
			
			//Generamos la Llamada
			$array = array(
				'msg' => 'getAllMediaApproved',
				'fields' => array(
					'offset' => $offset * 20
				),
				'app' => 'backend',
				'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
			);
			$json_array = json_encode($array);
			
			//Respuesta de la Llamada
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
			
			//Data
			if ($response_row['status'] == 1)
			{
				$data['total'] = $response_row['total'];
				$data['media'] = $response_row['data'];
			}
			else
			{
				$data['media'] = array();
			}
			$data['offset'] = $offset;
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('dashboard/images_view', $data);
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function waiting()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos la Variable
			$offset = $this->uri->segment(3,0);
			
			//Generamos la Llamada
			$array = array(
				'msg' => 'getAllMedia',
				'fields' => array(
					'offset' => $offset * 20
				),
				'app' => 'backend',
				'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
			);
			$json_array = json_encode($array);
			
			//Respuesta de la Llamada
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
			
			//Data
			if ($response_row['status'] == 1)
			{
				$data['total'] = $response_row['total'];
				$data['media'] = $response_row['data'];
			}
			else
			{
				$data['media'] = array();
			}
			$data['offset'] = $offset;
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('dashboard/waiting_view', $data);
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function messages()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos la Variable
			$offset = $this->uri->segment(3,0);
			
			//Generamos la Llamada
			$array = array(
				'msg' => 'getAllMessages',
				'fields' => array(
					'offset' => $offset * 20
				),
				'app' => 'backend',
				'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
			);
			$json_array = json_encode($array);
			
			//Respuesta de la Llamada
			$response = $this->functions->call($json_array);
			$response_row = json_decode($response, true);
			
			//Data
			if ($response_row['status'] == 1)
			{
				$data['mensajes'] = $response_row['data'];
			}
			else
			{
				$data['mensajes'] = array();
			}
			$data['offset'] = $offset;
			
			//Load Views
			$this->load->view('includes/header');
			$this->load->view('messages/index_view', $data);
			$this->load->view('includes/javascript');
			$this->load->view('includes/footer');
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function deleteMsg()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos el Valor
			$idwall_band = (isset($_POST['idwall_band'])) ? (int)$_POST['idwall_band'] : 0;
			
			//Verificamos
			if ($idwall_band != 0)
			{
				//Generamos la Llamada
				$array = array(
					'msg' => 'deleteMessage',
					'fields' => array(
						'idwall_band' => $idwall_band
					),
					'app' => 'backend',
					'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
				);
				$json_array = json_encode($array);
				
				//Respuesta de la Llamada
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
				
				//Data
				if ($response_row['status'] == 1)
				{
					//Regresamos Error
					echo 'success';
				}
				else
				{
					//Regresamos Error
					echo $response_row['msg'];
				}
			}
			else
			{
				//Regresamos Error
				echo 'error';
			}
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
	public function addMsg()
	{
		//Verify Login
		if ($this->session->userdata('corona_login'))
		{
			//Leemos el Valor
			$idwall_band = (isset($_POST['idwall_band'])) ? (int)$_POST['idwall_band'] : 0;
			
			//Verificamos
			if ($idwall_band != 0)
			{
				//Generamos la Llamada
				$array = array(
					'msg' => 'addMessage',
					'fields' => array(
						'idwall_band' => $idwall_band
					),
					'app' => 'backend',
					'apikey' => '04b57bc188977b3d10826a55fd9d1fdceae31575'
				);
				$json_array = json_encode($array);
				
				//Respuesta de la Llamada
				$response = $this->functions->call($json_array);
				$response_row = json_decode($response, true);
				
				//Data
				if ($response_row['status'] == 1)
				{
					//Regresamos Error
					echo 'success';
				}
				else
				{
					//Regresamos Error
					echo $response_row['msg'];
				}
			}
			else
			{
				//Regresamos Error
				echo 'error';
			}
		}
		else
		{	
			//Leemos Current URL
			$this->session->set_userdata('redirect', current_url());
			
			//Redirect Dashboard
			redirect(base_url());
		}
	}
	
}