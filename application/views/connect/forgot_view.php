
		<div class="container">
			<div class="row">
				<div class="span12 login">
					<div class="logo_connect">
						<img src="<?=base_url()?>images/cc.png" />
					</div>
					<div class="row">
						<div class="span3">&nbsp;</div>
						<div class="span6">
							<?php if ($this->session->userdata('forgot_success') != '') { ?>
								<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h4>Error</h4>
									<?=$this->session->userdata('forgot_success')?>
								</div>
							<?php } $this->session->set_userdata('forgot_success', ''); ?>
							<?php if ($this->session->userdata('forgot_error') != '') { ?>
								<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h4>Error</h4>
									<?=$this->session->userdata('forgot_error')?>
								</div>
							<?php } $this->session->set_userdata('forgot_error', ''); ?>
							<div class="form_connect well well-large">								
								<form id="form_forgot" name="form_forgot" method="post" action="<?=base_url()?>connect/resetPassword" accept-charset="utf-8" class="form-horizontal">
									<div class="control-group">
								    	<label class="control-label" for="inputEmail">Correo Electrónico</label>
										<div class="controls">
											<input class="input-block-level" autocomplete="off" type="text" id="inputEmail" name="inputEmail" placeholder="Escribe tu Correo Electrónico">
										</div>
									</div>
									<div class="center">
										<a id="btn_forgot" name="btn_forgot" href="" class="btn btn-large btn-primary"><strong><i class="icon-envelope icon-white"></i> Reiniciar Contraseña</strong></a>
									</div>
								</form>
							</div>
						</div>
						<div class="span3">&nbsp;</div>
					</div>
				</div>
			</div>
		</div>
		
		