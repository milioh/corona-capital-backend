
		<div class="container">
			<div class="row">
				<div class="span12 login">
					<div class="logo_connect">
						<img src="<?=base_url()?>images/cc.png" />
					</div>
					<div class="row">
						<div class="span3">&nbsp;</div>
						<div class="span6">
							<?php if ($this->session->userdata('error_login') != '') { ?>
								<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<h4>Error</h4>
									<?=$this->session->userdata('error_login')?>
								</div>
							<?php } $this->session->set_userdata('error_login', ''); ?>
							<div class="form_connect well well-large">								
								<form id="form_login" name="form_login" method="post" action="<?=base_url()?>connect/doLogin" accept-charset="utf-8" class="form-horizontal">
									<div class="control-group">
								    	<label class="control-label" for="inputUsername">Usuario</label>
										<div class="controls">
											<input class="input-block-level" autocomplete="off" type="text" id="inputUsername" name="inputUsername" placeholder="Escribe tu Nombre de Usuario">
										</div>
									</div>
									<div class="control-group">
								    	<label class="control-label" for="inputPassword">Contraseña</label>
										<div class="controls">
											<input class="input-block-level" autocomplete="off" type="password" id="inputPassword" name="inputPassword" placeholder="Escribe tu Contraseña">
											<span class="help-block">
												<a class="link" href="<?=base_url()?>connect/forgot">¿Olvidaste tu contraseña?</a>
											</span>
										</div>
									</div>
									<div class="center">
										<a id="btn_login" name="btn_login" href="" class="btn btn-large btn-primary"><strong><i class="icon-key icon-white"></i> Iniciar Sesión</strong></a>
									</div>
								</form>
							</div>
						</div>
						<div class="span3">&nbsp;</div>
					</div>
				</div>
			</div>
		</div>
		
		