
		<div class="container">
			<?php $this->load->view('includes/navbar_view'); ?>
			
			<div class="row with-navbar">
				<input type="hidden" id="base_url" name="base_url" value="<?=base_url()?>" />
				<?php foreach ($days as $day) { ?>
				<a href="<?=base_url()?>event/day/<?=$day['code']?>" target="_self">
					<div class="span3">
						<div class="well day">
							<i class="icon-calendar icon-4x"></i>
							<h4><?=$day['name']?></h4>
						</div>
					</div>
				</a>
				<?php } ?>
				<a href="<?=base_url()?>event/day/add" target="_self">
					<div class="span3">
						<div class="well new_event">
							<i class="icon-plus icon-4x"></i>
							<h2>Add Day</h2>
						</div>
					</div>
				</a>
			</div>
		</div>