
		<div class="container">
			<?php $this->load->view('includes/navbar_view'); ?>
			
			<div class="row" style="margin-top:60px !important;">
				<div class="span12">
					<?php if ($offset != 0) { ?>
					<div class="pull-left">
						<a id="prevMsg" name="prevMsg" href="<?=base_url()?>dashboard/messages/<?=$offset-1?>" class="btn">Anteriores</a>
						<br /><br />
					</div>
					<?php } ?>
					<?php if (isset($mensajes['next']) && $mensajes['next']==1) { ?>
					<div class="pull-right">
						<a id="nextMsg" name="nextMsg" href="<?=base_url()?>dashboard/messages/<?=$offset+1?>" class="btn">Siguientes</a>
						<br /><br />
					</div>
					<?php } ?>
				</div>
			</div>
			<?php if (isset($mensajes['messages']) && count($mensajes['messages']) > 0) { ?>
				<div class="row">
					<div class="span12">
						<?php foreach($mensajes['messages'] as $message) { ?>
						<?php
							$fondo_msg = ''; $status_string = 'Active';
							if ($message['status'] == 0) { $fondo_msg = 'msg-deleted'; $status_string = 'Deleted'; }
							if ($message['status'] == 1) { $fondo_msg = 'msg-normal'; $status_string = 'Active'; }
							if ($message['reported'] == 1) { $fondo_msg = 'msg-warning'; $status_string = 'Reportado'; }
						?>
						<div class="media well <?=$fondo_msg?>" id="msg_<?=$message['idwall_band']?>">
							<input type="hidden" id="base_url" name="base_url" value="<?=base_url()?>" />
							<a class="pull-left hidden-phone">
						    	<img src="<?=$message['user']['avatar']?>" class="media-object img-circle" data-src="holder.js/64x64">
							</a>
							<div class="media-band pull-right hidden-phone">
								<p style="font-weight:bold;"><?=$message['band']['name']?></p>
								<p>Status: <strong><?=$status_string?></strong></p>
							</div>
							<div class="media-body">
						    	<h4 class="media-heading"><?=$message['user']['name']?> <span class="label"><?=$message['timestamp_string']?></span></h4>
								
							    <!-- Nested media object -->
							    <div class="media">
							    	<p><?=$message['text']?></p>
							    </div>
							</div>
							<p class="visible-phone" style="font-weight:bold;"><?=$message['band']['name']?></p>
							<div class="media-buttons-delete<?=($fondo_msg=='msg-normal' || $fondo_msg=='msg-warning') ? ' mostrar' : '';?>" id="btn_delete_<?=$message['idwall_band']?>">
								<a id="deleteMsg" name="deleteMsg" rel="<?=$message['idwall_band']?>" href="" class="btn btn-danger"><i class="icon-trash"></i> Borrar Mensaje</a>
							</div>
							<div class="media-buttons-add<?=($fondo_msg=='msg-deleted') ? ' mostrar' : '';?>" id="btn_add_<?=$message['idwall_band']?>">
								<a id="addMsg" name="addMsg" rel="<?=$message['idwall_band']?>" href="" class="btn btn-success"><i class="icon-plus"></i> Republicar Mensaje</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>	
				<div class="row">
					<div class="span12">
						<?php if ($offset != 0) { ?>
						<div class="pull-left">
							<a id="prevMsg" name="prevMsg" href="<?=base_url()?>dashboard/messages/<?=$offset-1?>" class="btn">Anteriores</a>
							<br /><br />
						</div>
						<?php } ?>
						<?php if ($mensajes['next']==1) { ?>
						<div class="pull-right">
							<a id="nextMsg" name="nextMsg" href="<?=base_url()?>dashboard/messages/<?=$offset+1?>" class="btn">Siguientes</a>
							<br /><br />
						</div>
						<?php } ?>
					</div>
				</div>	
			<?php } ?>
		</div>