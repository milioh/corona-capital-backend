
		<div class="container">
			<?php $this->load->view('includes/navbar_view'); ?>
			
			<div class="row with-navbar">
				<input type="hidden" id="base_url" name="base_url" value="<?=base_url()?>" />
				<?php foreach ($events as $event) { ?>
				<a href="<?=base_url()?>event/details/<?=$event['code']?>" target="_self">
					<div class="span3">
						<div class="well event">
							<img class="img-circle logo_event" src="<?=$event['logo']?>" alt="<?=$event['name']?>" title="<?=$event['name']?>" />
							<h4><?=$event['name']?></h4>
						</div>
					</div>
				</a>
				<?php } ?>
				<a href="<?=base_url()?>event/add" target="_self">
					<div class="span3">
						<div class="well new_event">
							<i class="icon-plus icon-4x"></i>
							<h2>Add Event</h2>
						</div>
					</div>
				</a>
			</div>
		</div>