
		<div class="container">
			<?php $this->load->view('includes/navbar_view'); ?>
			
			<div class="row" style="margin-top:60px !important;">
				<div class="span3">
					<a href="<?=base_url()?>dashboard/images" class="btn btn-block btn-large btn-primary" id="btn_media"><i class="icon-picture"></i> Imágenes</a>
				</div>
				<div class="span3">
					<a href="<?=base_url()?>dashboard/waiting" class="btn btn-block btn-large btn-primary" id="btn_media"><i class="icon-picture"></i> Por Aprobar</a>
				</div>
				<div class="span3">
					<a href="<?=base_url()?>dashboard/messages" class="btn btn-block btn-large btn-primary" id="btn_media"><i class="icon-comment"></i> Mensajes</a>
				</div>
				<div class="span3">
					<a href="<?=base_url()?>connect/logout" class="btn btn-block btn-large btn-danger" id="btn_media"><i class="icon-power-off"></i> Cerrar Sesión</a>
				</div>
			</div>
		</div>
		