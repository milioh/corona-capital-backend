
		<div class="container">
			<?php $this->load->view('includes/navbar_view'); ?>
			
			<div class="row with-navbar">
				<div class="span12">
					<div class="text-center">
						<span style="color:#FFF;">Fotos Aprobadas: <?=$total?></span>
					</div>
					<?php if ($offset != 0) { ?>
					<div class="pull-left">
						<a id="prevImages" name="prevImages" href="<?=base_url()?>dashboard/images/<?=$offset-1?>" class="btn">Anteriores</a>
						<br /><br />
					</div>
					<?php } ?>
					<?php if (count($media)==20) { ?>
					<div class="pull-right">
						<a id="nextImages" name="nextImages" href="<?=base_url()?>dashboard/images/<?=$offset+1?>" class="btn">Siguientes</a>
						<br /><br />
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<input type="hidden" id="base_url" name="base_url" value="<?=base_url()?>" />
				<ul class="thumbnails">
				<?php $contador = 0; ?>
				<?php foreach ($media as $obj) { ?>
					<?php $contador++; ?>
					<li class="span4">
						<div class="thumbnail caja" id="thumb_<?=$obj['idphoto']?>">
							<input type="hidden" id="base_url" name="base_url" value="<?=base_url();?>" />
							<input type="hidden" id="scr_<?=$obj['idphoto']?>" name="scr_<?=$obj['idphoto']?>" value="<?=$obj['src']?>" />
							<?php if ($obj['type']=='image') { ?>
							<img src="<?=$obj['src']?>" data-src="<?=$obj['src']?>" alt="">
							<?php } else { ?>
							<video width="100%" controls>
							  <source src="<?=$obj['src']?>" type="video/mp4">
							  Your browser does not support the video tag.
							</video>
							<?php } ?>
							<p>
								<div class="pull-right" id="btn_media">
									<i><?=$obj['timestamp_string']?></i>
								</div>
							</p>
							<a id="link_media" href="<?=$obj['link']?>" target="_blank">
								<h3><img id="user_avatar" class="img-circle" width="30px" src="<?=$obj['user']['avatar']?>" /> <?=$obj['user']['name']?></h3>
							</a>
							<p>
								<?=$obj['text']?>
							</p>
							<p class="text-right">
								<span id="btn_approve_image_<?=$obj['idphoto']?>">
									<?php if ((int)$obj['status'] == 0) { ?>
									<a id="addImage" name="addImage" rel="<?=$obj['idphoto']?>" href="#" class="btn btn-success">Aprobar</a> 
									<?php } else { ?>
									<span class="label label-success">Foto Aprobada</span>
									<?php } ?>
								</span>
								<a id="deleteImage" name="deleteImage" rel="<?=$obj['idphoto']?>" href="#" class="btn btn-danger">Eliminar</a>
							</p>
						</div>
					</li>
				<?php } ?>
				</ul>
			</div>
			<div class="row">
				<div class="span12">
					<?php if ($offset != 0) { ?>
					<div class="pull-left">
						<a id="prevImages" name="prevImages" href="<?=base_url()?>dashboard/images/<?=$offset-1?>" class="btn">Anteriores</a>
						<br /><br />
					</div>
					<?php } ?>
					<?php if ($contador==20) { ?>
					<div class="pull-right">
						<a id="nextImages" name="nextImages" href="<?=base_url()?>dashboard/images/<?=$offset+1?>" class="btn">Siguientes</a>
						<br /><br />
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		