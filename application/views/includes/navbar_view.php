
			<div class="navbar navbar-fixed-top">
				<div class="navbar-inner">
			    	<div class="container">
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				        	<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
			 
						<a class="brand" href="<?=base_url()?>">CORONA CAPITAL</a>
			 
						<div class="nav-collapse collapse">
							<ul class="nav pull-right">
				                <li class="dropdown">
				                	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$this->session->userdata('corona_admin')['fullname']?> <b class="caret"></b></a>
				                    <ul class="dropdown-menu">
				                    	<li class="nav-header"><div class="text-center"><?=$this->session->userdata('corona_admin')['profile']['name']?></div></li>
				                    	<li><div class="text-center"><img src="<?=$this->session->userdata('corona_admin')['avatar']?>" class="img-circle avatar" /></div></li>
				                    	<li class="divider"></li>
				                    	<li><a href="<?=base_url()?>profile">Editar Perfil</a></li>
										<li class="divider"></li>
										<li><a href="<?=base_url()?>connect/logout">Cerrar Sesión</a></li>
				                    </ul>
				                </li>
				            </ul>
						</div>
					</div>
				</div>
			</div>