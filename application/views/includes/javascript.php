
		<!-- JavaScript plugins (requires jQuery) -->
	    <script src="<?=base_url()?>js/jquery.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="<?=base_url()?>js/bootstrap.min.js"></script>
	    <!-- Validate Form -->
	    <script src="<?=base_url()?>js/validate/jquery.validate.min.js"></script>
	    <script src="<?=base_url()?>js/validate/additional-methods.min.js"></script>
	    <script src="<?=base_url()?>js/validate/localization/messages_es.js"></script>
	    <!-- Funcions JS -->
	    <script src="<?=base_url()?>js/functions.js"></script>
	    