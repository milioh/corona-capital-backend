<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Corona Capital // BACKEND</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Bootstrap -->
		<link href="<?=base_url();?>css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="<?=base_url();?>css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?=base_url();?>css/font-awesome.min.css">
		<!-- Style -->
		<link href="<?=base_url();?>css/style.css" rel="stylesheet" media="screen">
	</head>
	<body>
		