
	// When ready...
	window.addEventListener("load",function() {
		// Set a timeout...
		setTimeout(function(){
			// Hide the address bar!
			window.scrollTo(0, 1);
		}, 0);
	});

	// Document Get Ready
	$( document ).ready(function() {
	
		// Validate Form Login
		var validator_form_login = $('#form_login').validate({
			rules: {
				inputUsername: {
					required: true
				},
				inputPassword: {
					required: true
				}
			}
		});
		
		// Button Login 
		$('#btn_login').live('click', function(e) {
			e.preventDefault();
			if ($('#form_login').valid())
			{
				//Post Form
				$('#form_login').submit();
			}
		});
		
		// Validate Form Forgot
		var validator_form_forgot = $('#form_forgot').validate({
			rules: {
				inputEmail: {
					required: true,
					email: true
				}
			}
		});
		
		// Button Forgot 
		$('#btn_forgot').live('click', function(e) {
			e.preventDefault();
			if ($('#form_forgot').valid())
			{
				//Post Form
				$('#form_forgot').submit();
			}
			return false;
		});
		
		// DASHBOARD
		$('#deleteImage').live('click', function(e) {
			e.preventDefault();
			
			//Leemos el ID
			var idphoto = $(this).attr('rel');
			var base_url = $('#base_url').val();
			
			//Generamos el POST
			$.post( base_url+"dashboard/deleteImage", { 'idphoto': idphoto }, function( data ) {
				//Verificamos
				if (data == 'success')
				{
					//Div Contenedor
					$('#thumb_'+idphoto).fadeOut('slow');
				}
				else
				{
					//Alerta de Error
					alert(data);
				}
			});
			
			return false;
		});
		
		// Agregamos Imagenes
		$('#addImage').live('click', function(e) {
			e.preventDefault();
			var idphoto = $(this).attr('rel');
			var base_url = $('#base_url').val();
			
			//Generamos el POST
			$.post( base_url+"dashboard/addImage", { 'idphoto': idphoto }, function( data ) {
				//Verificamos
				if (data == 'success')
				{
					//Mostramos el Label
					$('#btn_approve_image_'+idphoto).html('<span class="label label-success">Foto Aprobada</span>').fadeIn('slow');
				}
				else
				{
					//Alerta de Error
					alert(data);
				}
			});
		
			return false;
		});
		
		//MESSAGES
		
		// Borramos Mensaje
		$('#deleteMsg').live('click', function(e) {
			e.preventDefault();
			
			//Leemos el ID
			var idwall_band = $(this).attr('rel');
			var base_url = $('#base_url').val();
			
			//Generamos el POST
			$.post( base_url+"dashboard/deleteMsg", { 'idwall_band': idwall_band }, function( data ) {
				//Verificamos
				if (data == 'success')
				{
					//Div Contenedor
					$('#msg_'+idwall_band).removeClass('msg-normal');
					$('#msg_'+idwall_band).removeClass('msg-warning');
					$('#msg_'+idwall_band).addClass('msg-deleted');
					
					$('#btn_delete_'+idwall_band).removeClass('mostrar');
					$('#btn_add_'+idwall_band).addClass('mostrar');
				}
				else
				{
					//Alerta de Error
					alert(data);
				}
			});
			
			return false;
		});
		
		// Agregamos Mensajes
		$('#addMsg').live('click', function(e) {
			e.preventDefault();
			
			//Leemos el ID
			var idwall_band = $(this).attr('rel');
			var base_url = $('#base_url').val();
			
			//Generamos el POST
			$.post( base_url+"dashboard/addMsg", { 'idwall_band': idwall_band }, function( data ) {
				//Verificamos
				if (data == 'success')
				{
					//Div Contenedor
					$('#msg_'+idwall_band).removeClass('msg-deleted');
					$('#msg_'+idwall_band).removeClass('msg-warning');
					$('#msg_'+idwall_band).addClass('msg-normal');
					
					$('#btn_add_'+idwall_band).removeClass('mostrar');
					$('#btn_delete_'+idwall_band).addClass('mostrar');
				}
				else
				{
					//Alerta de Error
					alert(data);
				}
			});
			
			return false;
		});
		
	});